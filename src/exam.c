#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdbool.h>


int main(int argc, char *argv[])
{
    int opt;
    FILE *inputFile;          // Déclaration du descripteur de fichier
    void fcp(FILE *, FILE *); // Vérifie les fichiers
    bool is_print = 0;
    int ccase = 0;

    while ((opt = getopt(argc, argv, "picrs")) != -1)
    {
        switch (opt)
        {

        case 'p':

            is_print = true;
            ccase = 1;

            break;

        case 'i':

            is_print = true;
            ccase = 2;
            break;
        case 'c':

            is_print = true;
            ccase = 3;
            break;
        case 'r':

            is_print = true;
            ccase = 4;
            break;
        case 's':

            is_print = true;
            ccase = 5;
            break;
        }
    }

    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);

        if (is_print == true && ccase == 1){

            //Réussir à supprimer l'élément -p de l'argc :(

            if (argc == 1){
                    fcp(stdin, stdout); //Pas d'args, copie l'entrée standard
            }
            else{
                while (--argc > 0)
                    if ((inputFile = fopen(*++argv, "r")) == NULL){
                        printf("cat : impossible d'ouvrir %s\n", *argv); //Message d'erreur
                        return 1;
                    }
                    else {
                        fcp(inputFile, stdout); //Copie l'entrée standard dans la sortie standard
                        fclose(inputFile); //Ferme le descripteur
                        return 0;
                    }
            }
        }

        if (is_print == true && ccase == 2){
            
            
        }

        if (is_print == true && ccase == 3){
            
            
        }

        if (is_print == true && ccase == 4){
            
            
        }

        if (is_print == true && ccase == 5){
            
            
        }

    }

     for (; optind < argc; optind++)
    {
        // printf(“extra arguments: %s\n”, argv[optind]);

        if (is_print == true ){
            
            if (argc == 1){
                    fcp(stdin, stdout); //Pas d'args, copie l'entrée standard
            }
            else{
                while (--argc > 0)
                    if ((inputFile = fopen(*++argv, "r")) == NULL){
                        printf("cat : impossible d'ouvrir %s\n", *argv); //Message d'erreur
                        return 1;
                    }
                    else {
                        fcp(inputFile, stdout); //Copie l'entrée standard dans la sortie standard
                        fclose(inputFile); //Ferme le descripteur
                        return 0;
                    }
            }
        }
    return 0;
    }
}

void fcp(FILE *fpe, FILE *fps)
{           // fcp : copie le fichier fpe dans le fichier fps
    char c; // Variable temp
    while ((c = fgetc(fpe)) != EOF)
        fputc(c, fps); // Tant que le fichier fpe n'est pas vide, il écrit les caractères dans fps
}